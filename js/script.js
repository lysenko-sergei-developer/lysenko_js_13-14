'use strict';

$(function() {

  var quiz = {
    title: "Test of the C language",
    question: [
      {
        article: "What is the correct value to return to the operating system upon the successful completion of a program?",
        code: "",
        variant: [
          "1a",
          "1b",
          "1c",
          "1d"
        ],
        answer: [
          "-1",
          "1",
          "0",
          "Programs do not return a required value",
        ],
        rightAnswer: "1c",
      },
      {
        article: "What is the only function all C programs must contain?",
        code: "",
        variant: [
          "2a",
          "2b",
          "2c",
          "2d",
        ],
        answer: [
          "start()",
          "system()",
          "main()",
          "program()",
        ],
        rightAnswer: "2c",
      },
      {
        article: "Which of the following is not a correct variable type?",
        code: "",
        variant: [
          "3a",
          "3b",
          "3c",
          "3d",
        ],
        answer: [
          "float",
          "real",
          "int",
          "double",
        ],
        rightAnswer: "3b",
      },
      {
        article: "Which of the following is a correct comment?",
        code: "",
        variant: [
          "4a",
          "4b",
          "4c",
          "4d",
        ],
        answer: [
          "*/ comments */ and // comment",
          "// comments // and / comment",
          "/* comments */ and // comment",
          "<-- comments --> and / comment",
        ],
        rightAnswer: "4c",
      },
      {
        article: "Which statement can print \\n on the screen?",
        code: "",
        variant: [
          "5a",
          "5b",
          "5c",
          "5d",
        ],
        answer: [
          "printf(\"\\\\n\");",
          "printf(\"n\\\");",
          "printf(\"n\");",
          "printf(\'\\n\')",
        ],
        rightAnswer: "5a",
      },
      {
        article: "What is the output of the below code snippet?",
        code: "\n#include < stdio.h >\n\nmain()\n{\n\tint a = 5, b = 3, c = 4;\n\n\tprintf(\"a = %d, b = %d\\n\", a, b, c);\n}",
        variant: [
          "6a",
          "6b",
          "6c",
          "6d",
        ],
        answer: [
          "a=5, b=3",
          "a=5, b=3, c=0",
          "a=5, b=3, c=1",
          "compile error",
        ],
        rightAnswer: "6a",
      },
      {
        article: "What is the output of the following program?",
        code: "\n#include < stdio.h >\n\nmain()\n{\n\tint a[3] = {2,1};\n\n\tprintf(\"%d\", a[a[1]]);\n}",
        variant: [
          "7a",
          "7b",
          "7c",
          "7d",
        ],
        answer: [
          "0",
          "1",
          "2",
          "3",
        ],
        rightAnswer: "7b",
      },
      {
        article: "What is the output of the following program?",
        code: "\n#include < stdio.h >\n\nmain()\n{\n\tchar *s = \"Hello, \"\n\t\"World!\";\n\n\tprintf(\"%s\", s);\n}",
        variant: [
          "8a",
          "8b",
          "8c",
          "8d",
        ],
        answer: [
          "Hello, World!",
          "World!",
          "Hello",
          "compile error",
        ],
        rightAnswer: "8a",
      },
      {
        article: "What is the output of the following program?",
        code: "\n#include < stdio.h >\n\nmain()\n{\n\tstruct student\n\t{\n\t\tint num = 10;\n\t}var;\n\n\tprintf(\"%d\", var.num);\n}",
        variant: [
          "9a",
          "9b",
          "9c",
          "9d",
        ],
        answer: [
          "10",
          "garbage",
          "runtime error",
          "compile error",
        ],
        rightAnswer: "9a",
      },
      {
        article: "To store a word/sentence declare a variable of the type 'string'",
        code: "",
        variant: [
          "10a",
          "10b",
          "10c",
          "10d",
        ],
        answer: [
          "true",
          "false",
          "null",
          "not present in C"
        ],
        rightAnswer: "10b",
      },
    ],
  };

  var quizInString = JSON.stringify(quiz);
  localStorage.setItem('data', quizInString);

  var gems = localStorage.getItem('data');
  var newGems = JSON.parse(gems);

  var $quizGame = $('#quiz-game').html();
  var compiled = _.template($quizGame);
  var content = compiled(quiz);

  $('body').append(content);


  $('pre code').each(function(i, block) {
    hljs.highlightBlock(block);
  });

  $('#pass-test').click( function(event){ // лoвим клик пo ссылки с id="go"
		event.preventDefault(); // выключaем стaндaртную рoль элементa
    review();
		$('#overlay').fadeIn(400, // снaчaлa плaвнo пoкaзывaем темную пoдлoжку
		 	function(){ // пoсле выпoлнения предъидущей aнимaции
				$('#modal_form')
					.css('display', 'block') // убирaем у мoдaльнoгo oкнa display: none;
					.animate({opacity: 1, top: '50%'}, 200); // плaвнo прибaвляем прoзрaчнoсть oднoвременнo сo съезжaнием вниз
		});
	});
	/* Зaкрытие мoдaльнoгo oкнa, тут делaем тo же сaмoе нo в oбрaтнoм пoрядке */
	$('#modal_close, #overlay').click( function(){ // лoвим клик пo крестику или пoдлoжке
    uncheckedAll();
    $('#modal_form')
			.animate({opacity: 0, top: '45%'}, 200,  // плaвнo меняем прoзрaчнoсть нa 0 и oднoвременнo двигaем oкнo вверх
				function(){ // пoсле aнимaции
					$(this).css('display', 'none'); // делaем ему display: none;
					$('#overlay').fadeOut(400); // скрывaем пoдлoжку
				}
			);
	});

  function review() {
    var $answers = $('.answersItem');

    // pull user answers
    var userAnswers = [];
    for (var item of $answers) {
        if (item.checked == true) {
          userAnswers.push(item.id);
        };
    };

    // check whether all the questions answered
    var $returnResult = $('.send_to_user');
    if(userAnswers.length < 9) {
      $returnResult.text('please fill in all fields!');
    };

    // pull right answers
    var rightAnswers = [];
    quiz.question.forEach(function(item) {
      rightAnswers.push(item.rightAnswer);
    });

    //compare userAnswers and rightAnswers
    //if properly more 80%, test is pass
    var correctAnswers = 0;
    for (var i = 0; i < userAnswers.length; i++) {
      if (userAnswers[i] == rightAnswers[i]) {
        correctAnswers += 1;
      }
    }

    //chech user result and tell him it
    if (correctAnswers >= 8) {
      $returnResult.text('You good know C langue, your result ' +
                         correctAnswers + '/10!');
    } else {
      $returnResult.text('You fail :( your result ' +
                         correctAnswers + '/10')
    }
  };

  function uncheckedAll() {
    var $allItems = $('.answersItem');
    for (var iter of $allItems) {
      iter.checked = false;
    }
  }
});
